#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "i_circular_buffer_repository.h"
#include "circular_buffer_infra_file_service.h"
#include "i_circular_buffer.h"
#define INDEX_SUFFIX ".ndx"
#define DATA_SUFFIX ".rec"
#define FILE_DB_REPO "../Persistence/FileDB/CircularBuffer/CIRCULAR_BUFFER"

struct save_buffer
{
    unsigned long data_length;
    int head;
    int current;
    char data[]; //new in c99 !!
};

struct circular_buffer
{
    char *tail;
    unsigned long length;
    char *head;
    char *current;
    bool isFull;
};

struct index
{
    long recordStart;
    size_t recordLength;
};

static FILE *index_stream;
static FILE *data_stream;

int ICircularBufferRepository_save(circular_buffer circular_buffer)
{
    if (!ICircularBufferRepository_open(FILE_DB_REPO)) {
        return 0;
    }
    struct index index;
    struct save_buffer record;

    record.data_length = circular_buffer->length;
    record.current = (circular_buffer->current - circular_buffer->tail) / sizeof(char);
    
    if (circular_buffer->isFull) {
        record.head = circular_buffer->length;
    }
    else {
        record.head = (circular_buffer->head - circular_buffer->tail) / sizeof(char);
    }
    
    for (int i=0; i<record.head; i++) {
        record.data[i] = circular_buffer->tail[i];
    }

    fseek(data_stream, 0L, SEEK_END);

    int recordLength = sizeof(record) + sizeof(char) * record.head;
    
    index.recordStart = ftell(data_stream);
    index.recordLength = recordLength;

    fwrite(&record, recordLength, 1, data_stream);
    fseek(index_stream, 0L, SEEK_END);
    fwrite(&index, sizeof(index), 1, index_stream);

    return 1;
}

void ICircularBufferRepository_close(void)
{
    fclose(index_stream);
    fclose(data_stream);
}

static FILE *auxiliary_open(char *prefix, char *suffix)
{
    int prefix_length = (int)strlen(prefix);
    int suffix_length = (int)strlen(suffix);
    char name[prefix_length + suffix_length + 1];
    strncpy(name, prefix, prefix_length);
    strncpy(name + prefix_length, suffix, suffix_length + 1);
    FILE *stream = fopen(name, "r+");
    if (stream == NULL)
        stream = fopen(name, "w+");
    if (stream == NULL)
        perror(name);
    return stream;
}

int ICircularBufferRepository_open(char *name)
{
    data_stream = auxiliary_open(name, DATA_SUFFIX);
    if (data_stream == NULL)
        return 0;

    index_stream = auxiliary_open(name, INDEX_SUFFIX);
    if (index_stream == NULL) {
        fclose(data_stream);
        return 0;
    }

    return 1;
}

circular_buffer ICircularBufferRepository_get_nth_cb(int rank) {
    struct index index;
    long shift = (rank - 1) * sizeof(index);
    fseek(index_stream, shift, SEEK_SET);
    fread(&index, sizeof(index), 1, index_stream);

    fseek(data_stream, index.recordStart, SEEK_SET);

    struct save_buffer record;
    fread(&record.data_length, sizeof(unsigned long), 1, data_stream);
    fread(&record.head, sizeof(int), 1, data_stream);
    fread(&record.current, sizeof(int), 1, data_stream);
    fread(&record.data, sizeof(char) * record.head, 1, data_stream);

    circular_buffer circular_buffer = CircularBuffer_construct(record.data_length);

    for (int i =0; i < record.head; i++) {
        CircularBuffer_append_char_at_head(circular_buffer, record.data[i]);
    }

    circular_buffer->current = circular_buffer->tail + record.current;

    return circular_buffer;
}
